// @vihkoon(horse-test)

describe('Horse test', () => {
   const horse = {
      legs: 4,
      big: true,
      eatsGrass: false,
   }

   test("Horse has 4 legs. If a horse is in an car accident, it might lose one of it's legs. If this happens, the horse will have 3 legs left.", () => {
      expect(horse.legs).toEqual(4)
   })

   test('Horse is big', () => expect(horse.big).toBeTruthy())

   describe('Eating', () => {
      test('Horse eats grass', () => {
         expect(horse.eatsGrass).toBeTruthy()
      })
   })
})

describe('Horse test 2', () => {
   const horse = {
      legs: 4,
      big: true,
      eatsGrass: false,
   }

   test.todo('Horse has 4 legs')

   test.skip('Horse is big', () => expect(horse.big).toBeTruthy())

   describe('Eating', () => {
      test('Horse eats grass', () => {
         expect(horse.eatsGrass).toBeTruthy()
      })
   })
})
