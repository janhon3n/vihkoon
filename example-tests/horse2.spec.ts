// @vihkoon(horse-test)

describe('Another horse test from another file', () => {
   const horse = {
      legs: 4,
      big: true,
      eatsGrass: false,
   }

   test('Horse has 4 legs', () => {
      expect(horse.legs).toEqual(4)
   })

   test('Horse is big', () => expect(horse.big).toBeTruthy())

   describe('Eating', () => {
      test('Horse eats grass', () => {
         expect(horse.eatsGrass).toBeTruthy()
      })
   })
})
