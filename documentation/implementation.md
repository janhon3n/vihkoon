# How to implement vihkoon

## serve
Express serves content of the documentation directory.

Query parameters control how the content is returned:
- no parameters = Return full html page with header and side navigation, and content loaded to the main view
- htmlContent=true = Return only the content rendered as html
- pureContent=true = Return the pure content as is


## build
The files in the sourceDir are tranpiled into html content and placed into destDir/_content. For each file, a html page is created with a nav tree and the html content. Nav tree links load content from the _content folder. The non-html are also placed into the destDir in their pure form, so they can be used in the html content (images for example) or open separately (pdfs for example).
