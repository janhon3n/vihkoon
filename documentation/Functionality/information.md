# Information
Some information in markdown

## Lists
- item1
- item2

## Table
| animal | number of legs | can fly? |
|--------|----------------|----------|
| cat    | 4              | no       |
| duck   | 2              | yes      |
| snake  | 0              | nooou    |
