document.addEventListener(
   'DOMContentLoaded',
   function () {
      const directoryLinks = document.querySelectorAll('nav li.directory')
      directoryLinks.forEach((link) => {
         link.addEventListener('click', (event) => {
            const subGroup = link.nextElementSibling
            const closed = subGroup.getAttribute('closed')
            subGroup.setAttribute(
               'closed',
               closed === 'true' ? 'false' : 'true'
            )
         })
      })

      const fileLinks = document.querySelectorAll('nav li.file')
      fileLinks.forEach((link) => {
         link.addEventListener('click', (event) => {
            const title = link.getAttribute('title')
            const urlPath = link.getAttribute('urlPath')
            const contentPath = link.getAttribute('contentPath')
            if (urlPath.split('.').pop() === 'pdf') {
               window.open(urlPath, '_blank');
            } else {
               loadContent(title, urlPath, contentPath)
            }
         })
      })
   },
   false
)

function loadContent(title, urlPath, contentPath) {
   history.pushState({}, title, urlPath)
   console.info('Fetching content ' + contentPath)

   document.getElementById('loading').setAttribute('active', 'true')

   var xhr = new XMLHttpRequest()
   xhr.open('GET', contentPath, true)
   xhr.onreadystatechange = function () {
      if (this.readyState !== 4) return
      if (this.status !== 200) return

      console.info('Received content ' + contentPath)
      document.querySelectorAll('main')[0].innerHTML = this.responseText
      document.getElementById('loading').setAttribute('active', 'false')
   }
   xhr.send()
}
