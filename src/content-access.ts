import fs from 'fs'

export async function readFileContent(path: string): Promise<Buffer> {
   const content = await new Promise<Buffer>((resolve, reject) => {
      fs.readFile(path, (err, data) => {
         if (err) reject(err)
         else resolve(data)
      })
   })

   return content
}
