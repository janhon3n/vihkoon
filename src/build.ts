import * as fs from 'fs'
import * as path from 'path'
import npc from 'ncp'
import { getTree, TreeNode } from './tree'
import { createHtmlPage } from './create-html-page'
import { convertToHtml } from './convert-to-html'
import ncp from 'ncp'

async function generateHtmlFiles(options: {
   sourceDir: string
   destDir: string
   node: TreeNode
   tree: TreeNode
}) {
   const { destDir, node, sourceDir, tree } = options

   const sourcePath = path.join(sourceDir, node.path)
   console.info('Handling ' + sourcePath)

   let destContentPath = path.join(destDir, node.contentPath)
   let destFilePath = path.join(destDir, node.urlPath)

   if (node.type === 'file') {
      console.info(node)
      const htmlContent = await convertToHtml({
         absolutePath: sourcePath,
         relativePath: node.path,
         filetype: node.extension,
      })
      fs.writeFileSync(destContentPath, htmlContent)

      const page = createHtmlPage({
         content: htmlContent,
         navigationTree: tree,
      })

      fs.writeFileSync(destFilePath, page)
      if (node.extension !== '.html' && node.extension !== '.htm') {
         fs.copyFileSync(
            path.join(sourceDir, node.path),
            path.join(destDir, node.path)
         )
      }
   } else if (node.type === 'directory') {
      if (!fs.existsSync(destFilePath)) fs.mkdirSync(destFilePath)
      if (!fs.existsSync(destContentPath)) fs.mkdirSync(destContentPath)
   }

   for (const child of node.children || []) {
      generateHtmlFiles({
         node: child,
         destDir,
         sourceDir,
         tree,
      })
   }
}

export function build({ sourceDir, destDir }) {
   if (!fs.existsSync(destDir)) {
      fs.mkdirSync(destDir)
   }

   console.info('Analysing filetree...')
   const filetree = getTree(sourceDir, (node: TreeNode) => {
      if (node.type === 'file') {
         node.urlPath =
            node.path.substr(0, node.path.lastIndexOf('.')) + '.html'
         node.contentPath =
            '/_content' +
            node.path.substr(0, node.path.lastIndexOf('.')) +
            '.html'
      } else {
         node.contentPath = '_content' + node.path
         node.urlPath = node.path
      }
   })

   if (!fs.existsSync(path.join(destDir, '_content')))
      fs.mkdirSync(path.join(destDir, '_content'))

   console.info('Generating html files...')
   generateHtmlFiles({
      destDir,
      sourceDir,
      node: filetree,
      tree: filetree,
   })

   console.info('Copying static assets...')
   const staticDirSource = path.join(__dirname, 'template', 'static')

   ncp.ncp(staticDirSource, path.join(destDir, '_static'), (err) => {
      if (err) throw err
   })

   console.info('Documentation build succesfully into ' + destDir)
}
