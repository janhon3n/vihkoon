import express from 'express'
import * as path from 'path'
import * as fs from 'fs'
import { createHtmlPage } from './create-html-page'
import { getTree, TreeNode } from './tree'
import { readFileContent } from './content-access'
import { convertToHtml, supportedFiletypes } from './convert-to-html'

export function serve({ sourceDir, port }) {
   const server = express()
   server.use(
      '/_static',
      express.static(path.join(__dirname, 'template', 'static'))
   )

   const nodeTransform = (node: TreeNode) => {
      node.contentPath = node.path + '?htmlContent=true'
      node.urlPath = node.path
   }

   server.get('/', (req, res, next) => {
      fs.exists(path.join(sourceDir, 'index.md'), (exists) => {
         if (exists) {
            res.redirect('index.md')
         } else {
            fs.exists(path.join(sourceDir, 'index.html'), (exists) => {
               if (exists) {
                  res.redirect('index.html')
               } else {
                  res.send(
                     createHtmlPage({
                        content: 'No index.md or index.html found',
                        navigationTree: getTree(sourceDir, nodeTransform),
                     })
                  )
               }
            })
         }
      })
   })

   // Serve these files raw
   server.get(/\.(png|gif|jpg|pdf|svg)$/, (req, res) => {
      let filepath = path.join(sourceDir, req.path)
      filepath = decodeURI(filepath)
      console.info(`Serving ${filepath} in pure form`)

      res.sendFile(filepath)
   })

   server.get(supportedFiletypes, (req, res, next) => {
      const htmlContent = !!req.query?.htmlContent
      const pureContent = !!req.query?.pureContent

      let filepath = path.join(sourceDir, req.path)
      filepath = decodeURI(filepath)

      if (pureContent) {
         res.sendFile(filepath)
         return
      }

      convertToHtml({
         absolutePath: filepath,
         relativePath: req.path,
         filetype: '.' + req.path.split('.').pop(),
      }).then((html) => {
         if (htmlContent) {
            res.send(html)
            return
         }
         const page = createHtmlPage({
            content: html,
            navigationTree: getTree(sourceDir, nodeTransform),
         })
         res.send(page)
      })
   })

   server.listen(port)
   console.info(`Serving the documentation on port ${port}`)
}
