#!/usr/bin/env node

const meow = require('meow')
const path = require('path')
import * as fs from 'fs'
import { serve } from './serve'
import { build } from './build'

const cli = meow(
   `
Usage
$ vihkoon serve
$ vihkoon build

`,
   {
      flags: {
         port: {
            default: 3333,
         },
         sourceDir: {
            default: 'documentation',
         },
         destDir: {
            default: 'vihkoon-build',
         },
      },
   }
)

let sourceDir = cli.flags.sourceDir
sourceDir = path.join(process.cwd(), sourceDir)

let destDir = cli.flags.destDir
destDir = path.join(process.cwd(), destDir)

if (!fs.existsSync(sourceDir))
   throw Error('Source directory ' + sourceDir + ' does not exist')

if (cli.input[0] === 'serve' || cli.input.length === 0) {
   serve({
      port: cli.flags.port,
      sourceDir,
   })
} else if (cli.input[0] === 'build') {
   build({
      sourceDir,
      destDir,
   })
} else {
   throw Error('Invalid command. See alivecode --help.')
}
