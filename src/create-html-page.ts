import * as pug from 'pug'
import * as path from 'path'
import { TreeNode } from './tree'

const templateFunction = pug.compileFile(
   path.join(__dirname, 'template', 'index.pug')
)

export function createHtmlPage(options: {
   navigationTree: TreeNode
   content: string
}) {
   return templateFunction({
      documentationTitle: 'Example Documentation',
      navigationTree: options.navigationTree,
      content: options.content,
   })
}
