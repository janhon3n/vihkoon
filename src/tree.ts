import dirTree from 'directory-tree'

export interface TreeNode {
   path: string
   name: string
   type: 'file' | 'directory'
   children?: TreeNode[]
   extension?: string
   contentPath?: string
   urlPath?: string
}

function cleanupTree(
   node: TreeNode,
   sourceDirectory: string,
   nodeTransform?: (node: TreeNode) => void
) {
   node.path = node.path.replace(sourceDirectory, '')
   if (nodeTransform) nodeTransform(node)

   if (node.children) {
      for (const child of node.children) {
         cleanupTree(child, sourceDirectory, nodeTransform)
      }
   }
}

export function getTree(
   sourceDirectory: string,
   nodeTransform: (node: TreeNode) => void
) {
   const tree = dirTree(sourceDirectory, {
      extensions: /\.(html|htm|md|plantuml|pdf)$/,
   })
   cleanupTree(tree, sourceDirectory, nodeTransform)
   return tree
}
