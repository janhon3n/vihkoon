import {
   Reporter,
   Context,
   Test,
   TestResult,
   AggregatedResult,
   ReporterOnStartOptions,
} from '@jest/reporters'
import { AssertionResult } from '@jest/test-result'

import * as pug from 'pug'
import * as fs from 'fs'
import * as path from 'path'

const regex = /@vihkoon\(.*\)/

const stripAnsi = require('strip-ansi')

const template = fs
   .readFileSync(path.join(__dirname, 'template', 'test-report.pug'))
   .toString()
const generatePage = pug.compile(template)

interface TestGroup {
   description: string
   tests: AssertionResult[]
}

const testFiles = {}

function groupTestsByDescription(tests: AssertionResult[]): TestGroup[] {
   const groups = []
   for (const test of tests) {
      const group = test.ancestorTitles[0]
      if (group && !groups.includes(group)) groups.push(group)
   }

   return groups.map((group) => {
      return {
         description: group,
         tests: tests.filter((test) => test.ancestorTitles[0] === group),
      }
   })
}

function createFileReport(
   file: TestResult,
   fileContents: string,
   outputDir: string
) {
   let outputPath = fileContents.match(regex)[0]
   outputPath = outputPath.substring(9, outputPath.length - 1)
   if (!outputPath.endsWith('.html')) outputPath = outputPath + '.html'
   outputPath = path.join(outputDir, outputPath)

   const testGroups = groupTestsByDescription(file.testResults)
   for (const test of file.testResults) {
      //@ts-ignore
      test.failureMessage = stripAnsi(test.failureMessages[0])
      test.failureMessages = null
   }

   const html = generatePage({
      data: file,
      filename: path.basename(file.testFilePath),
      testGroups: testGroups,
      fileContents: fileContents,
   })

   if (!testFiles[outputPath]) {
      testFiles[outputPath] = ''
   }
   testFiles[outputPath] += html
}

function createHtmlTestReport(results: AggregatedResult, outputDir: string) {
   for (const file of results.testResults) {
      const fileContents = fs.readFileSync(file.testFilePath).toString()
      if (regex.test(fileContents))
         createFileReport(file, fileContents, outputDir)
   }

   for (const outputPath of Object.keys(testFiles)) {
      fs.writeFileSync(outputPath, testFiles[outputPath])
   }
}

interface Options {
   ouputDir: string
}

export default class MarkdownReporter implements Reporter {
   options: Options

   constructor(globalConfig, options) {
      this.options = {
         ouputDir: options.ouputDir || 'documentation',
      }
   }

   onTestResult(
      test: Test,
      testResult: TestResult,
      aggregatedResult: AggregatedResult
   ) {}

   onRunStart(results: AggregatedResult, options: ReporterOnStartOptions) {}

   onTestStart(test: Test) {}

   onRunComplete(contexts: Set<Context>, results: AggregatedResult) {
      createHtmlTestReport(results, this.options.ouputDir)
   }

   getLastError: () => void | Error
}
