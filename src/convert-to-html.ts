import * as showdown from 'showdown'
import fs from 'fs'
const markdownConverter = new showdown.Converter({ tables: true })
import pdf2html from 'pdf2html'
import { readFileContent } from './content-access'
import plantuml from 'node-plantuml'

export const supportedFiletypes = /\.(html|htm|md|plantuml)$/

export async function convertToHtml(input: {
   absolutePath: string
   relativePath: string
   filetype: string
}): Promise<string> {
   const { absolutePath, relativePath, filetype } = input

   let content
   switch (filetype) {
      case '.md':
         content = await readFileContent(absolutePath)
         let html = markdownConverter.makeHtml(content.toString()) as string
         const matches = html.match(/<img src=\"\w*\"/g)
         //TODO replace matches with images???
         return html

      case '.html':
      case '.htm':
         content = await readFileContent(absolutePath)
         return content.toString()

      case '.plantuml':
         var gen = plantuml.generate(absolutePath, { format: 'svg' })

         const absoluteImageFilePath =
            absolutePath.substr(0, absolutePath.lastIndexOf('.')) + '.svg'
         const relativeImageFilePath =
            relativePath.substr(0, relativePath.lastIndexOf('.')) + '.svg'

         var outputFile = fs.createWriteStream(absoluteImageFilePath)
         gen.out.pipe(outputFile)
         await new Promise((resolve, reject) => {
            gen.out.on('end', () => resolve())
         })

         return `
         <div>
            <img src="${relativeImageFilePath}" />
         </div>
         `

      default:
         throw Error('Invalid filetype ' + filetype)
   }
}
